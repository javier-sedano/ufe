WARNING: early alpha, do not use unless you know whta you are doing

PoC of a microfrontend architectecture using single-spa

Keywords: microfrontend, single-spa

# Instructions

- Use `yarn dependenciesInit` to install dependencies.
- Use `yarn start` to run the root project (host).
- You can run independently each project using `yarn start:<project>`.
- Browse http://localhost:9000
- Test SSR with `curl http://localhost:9000` and `curl http://localhost:9000/nonexisting` (currently does not work).

# Useful links

- https://single-spa.js.org/

TODO: CSP, CI/CD, separate repo apps

Not working:

- Suggested: use containerized development. Install VSCode; install Docker (either Docker Desktop or Rancher Desktop with docker CLI); install Dev Container extension in VSCode; then either clone and open (VSCode will offer to reopen in container) or directly clone-in-container.
