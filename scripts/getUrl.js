const http = require("http");
const { exit } = require("process");

const url = process.argv[2];
if (!url) {
  console.error("Usage: node getUrl.js <url>");
  exit(-1);
}

http
  .get(url, function (response) {
    response.pipe(process.stdout);
  })
  .on("error", (err) => {
    console.error(err);
  });
